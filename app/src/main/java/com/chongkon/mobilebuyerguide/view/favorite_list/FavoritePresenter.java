package com.chongkon.mobilebuyerguide.view.favorite_list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.chongkon.mobilebuyerguide.service.model.MobileModel;
import com.chongkon.mobilebuyerguide.tools.Enum;
import com.chongkon.mobilebuyerguide.tools.SortHelper;
import com.chongkon.mobilebuyerguide.view.main.MainPresenter;
import com.chongkon.mobilebuyerguide.view.mobile_list.MobileAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;

/*
 * Created by omkingo on 02/10/18.
 */
public class FavoritePresenter extends MobileAdapter.OnItemClickListener implements IFavoriteList.Presenter {

    private IFavoriteList.View mView;
    private ArrayList<MobileModel> mList;

    @Override
    public void attachView(IFavoriteList.View view) {
        mView = view;
        mList = new ArrayList<>();
    }

    @Override
    public void onInit() {
        mView.setFavoriteTab();
        onRegisterBroadCast();
    }

    private BroadcastReceiver mSortBroadCast = new BroadcastReceiver() {
        @Override public void onReceive(Context context, Intent intent) {
            SortHelper.onSortMobileList(intent.getIntExtra(Enum.Bundle.SORT_SELECTED, -1), mList, new SortHelper.OnSortListener(){
                @Override
                public void onSortCompleted(ArrayList<MobileModel> sortedList) {
                    mView.onSetListData(sortedList);
                }
            });
        }
    };

    private BroadcastReceiver mRemoveFavoriteBroadCast = new BroadcastReceiver() {
        @Override public void onReceive(Context context, Intent intent) {
            MobileModel mobileModel = Parcels.unwrap(intent.getParcelableExtra(Enum.Bundle.MOBILE_MODEL));

            if (mobileModel != null){
                if (intent.getBooleanExtra(Enum.Bundle.IS_ADD_FAVORITE, true)){
                    mList.add(mobileModel);
                }else {
                    mList.remove(mobileModel);
                }

                SortHelper.onSortMobileList(MainPresenter.SORT_TYPE, mList, new SortHelper.OnSortListener(){
                    @Override
                    public void onSortCompleted(ArrayList<MobileModel> sortedList) {
                        mView.onSetListData(sortedList);
                    }
                });
            }

        }
    };

    private void onRegisterBroadCast() {
        LocalBroadcastManager.getInstance(mView.getContext()).registerReceiver(mSortBroadCast, new IntentFilter(Enum.REGISTER_SORT));
        LocalBroadcastManager.getInstance(mView.getContext()).registerReceiver(mRemoveFavoriteBroadCast, new IntentFilter(Enum.REGISTER_ADD_REMOVE_FAV));
    }

    @Override
    public ItemTouchHelper.SimpleCallback getSwipeListener(){
        return new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                Intent sort_intent = new Intent(Enum.REGISTER_FAVORITE_REMOVED);
                sort_intent.putExtra(Enum.Bundle.MOBILE_MODEL, Parcels.wrap(mList.get(viewHolder.getAdapterPosition())));
                LocalBroadcastManager.getInstance(mView.getContext()).sendBroadcast(sort_intent);

                //Remove swiped item from list and notify the RecyclerView
                mList.remove(viewHolder.getAdapterPosition());
                mView.onSetListData(mList);
            }
        };
    }

    @Override
    public void onItemClicked(MobileModel model) {
        mView.onMobileDetailActivity(model);
    }

    @Override
    public MobileAdapter.OnItemClickListener getAdapterListener() {
        return this;
    }

    @Override
    public void detachView() {
        LocalBroadcastManager.getInstance(mView.getContext()).unregisterReceiver(mSortBroadCast);
        LocalBroadcastManager.getInstance(mView.getContext()).unregisterReceiver(mRemoveFavoriteBroadCast);
        mView = null;
    }

}
