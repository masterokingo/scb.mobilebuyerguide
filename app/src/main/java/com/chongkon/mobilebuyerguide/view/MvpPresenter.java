package com.chongkon.mobilebuyerguide.view;

public interface MvpPresenter<V> {

    void attachView(V view);

    void detachView();

}
