package com.chongkon.mobilebuyerguide.view.mobile_list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.chongkon.mobilebuyerguide.service.ApiServiceHelper;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;
import com.chongkon.mobilebuyerguide.tools.Enum;
import com.chongkon.mobilebuyerguide.tools.SortHelper;

import org.parceler.Parcels;

import java.util.ArrayList;

/*
 * Created by omkingo on 02/10/18.
 */
public class MobilePresenter extends MobileAdapter.OnItemClickListener implements IMobile.Presenter {

    private IMobile.View mView;
    private ArrayList<MobileModel> mList;
    private ApiServiceHelper mApiHelper;

    @Override
    public void attachView(IMobile.View view) {
        mView = view;
        mApiHelper = new ApiServiceHelper(mView.getContext());
        mList = new ArrayList<>();
    }

    @Override
    public void onInit(){
        mView.showLoading();
        mApiHelper.callGetMobileList(new ApiServiceHelper.ApiCallback(){
            @Override
            public void onResponse(ArrayList<?> model) {
                mView.hideLoading();
                if (model.size() > 0) {
                    mList = (ArrayList<MobileModel>) model;

                    mView.onSetListData(mList);
                }
            }
        });

        onRegisterBroadcast();
    }

    private BroadcastReceiver mSortBroadcast = new BroadcastReceiver() {
        @Override public void onReceive(Context context, Intent intent) {
            SortHelper.onSortMobileList(intent.getIntExtra(Enum.Bundle.SORT_SELECTED, -1), mList, new SortHelper.OnSortListener() {
                @Override
                public void onSortCompleted(ArrayList<MobileModel> sortedList) {
                    mView.onSetListData(sortedList);
                }
            });
        }
    };

    private BroadcastReceiver mRemoveFavorite = new BroadcastReceiver() {
        @Override public void onReceive(Context context, Intent intent) {
            MobileModel mobileModel = Parcels.unwrap(intent.getParcelableExtra(Enum.Bundle.MOBILE_MODEL));

            for (MobileModel model: mList){
                if (model.equals(mobileModel))
                    model.isFavorite = false;
            }
            mView.onSetListData(mList);
        }
    };

    private void onRegisterBroadcast() {
        LocalBroadcastManager.getInstance(mView.getContext()).registerReceiver(mSortBroadcast, new IntentFilter(Enum.REGISTER_SORT));
        LocalBroadcastManager.getInstance(mView.getContext()).registerReceiver(mRemoveFavorite, new IntentFilter(Enum.REGISTER_FAVORITE_REMOVED));

    }

    @Override
    public void onItemClicked(MobileModel model) {
        mView.onMobileDetailActivity(model);
    }

    @Override
    public void onFavoriteClicked(MobileModel model) {
        onSendBoardCastFavorite(model, true);
    }

    @Override
    public void onRemoveFavorite(MobileModel model) {
        onSendBoardCastFavorite(model, false);
    }

    private void onSendBoardCastFavorite(MobileModel model, boolean isAdded){
        Intent sort_intent = new Intent(Enum.REGISTER_ADD_REMOVE_FAV);
        sort_intent.putExtra(Enum.Bundle.MOBILE_MODEL, Parcels.wrap(model));
        sort_intent.putExtra(Enum.Bundle.IS_ADD_FAVORITE, isAdded);
        LocalBroadcastManager.getInstance(mView.getContext()).sendBroadcast(sort_intent);
    }

    @Override
    public MobileAdapter.OnItemClickListener getAdapterListener() {
        return this;
    }

    @Override
    public void detachView() {
        LocalBroadcastManager.getInstance(mView.getContext()).unregisterReceiver(mSortBroadcast);
        LocalBroadcastManager.getInstance(mView.getContext()).unregisterReceiver(mRemoveFavorite);
        mView = null;
        mApiHelper.onDispose();
    }

}
