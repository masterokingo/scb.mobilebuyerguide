package com.chongkon.mobilebuyerguide.view.mobile_detail;

import com.chongkon.mobilebuyerguide.service.ApiServiceHelper;
import com.chongkon.mobilebuyerguide.service.model.ImageModel;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;

import java.util.ArrayList;

/*
 * Created by omkingo on 02/10/18.
 */
public class MobileDetailPresenter implements IMobileDetail.Presenter {

    private IMobileDetail.View mView;
    private ApiServiceHelper mApiHelper;

    @Override
    public void attachView(IMobileDetail.View view) {
        mView = view;
        mApiHelper = new ApiServiceHelper(mView.getContext());
    }

    @Override
    public void onInitUi(MobileModel model) {
        if (model != null){
            mView.setUi();

            mView.showLoading();
            mApiHelper.callGetImagePath(model.id, new ApiServiceHelper.ApiCallback(){
                @Override
                public void onResponse(ArrayList<?> model) {
                    mView.hideLoading();
                    mView.setImageData((ArrayList<ImageModel>) model);
                }
            });
        }
    }

    @Override
    public void detachView() {
        mView = null;
        mApiHelper.onDispose();
    }


}
