package com.chongkon.mobilebuyerguide.view.favorite_list;


import android.support.v7.widget.helper.ItemTouchHelper;

import com.chongkon.mobilebuyerguide.service.model.MobileModel;
import com.chongkon.mobilebuyerguide.view.MvpPresenter;
import com.chongkon.mobilebuyerguide.view.MvpView;
import com.chongkon.mobilebuyerguide.view.mobile_list.MobileAdapter;

import java.util.ArrayList;

/*
 * Created by omkingo on 2/10/18.
 */

public interface IFavoriteList {


    interface View extends MvpView{
        void onSetListData(ArrayList<MobileModel> list);

        void setFavoriteTab();

        void onMobileDetailActivity(MobileModel model);
    }

    interface Presenter extends MvpPresenter<IFavoriteList.View> {
        ItemTouchHelper.SimpleCallback getSwipeListener();

        MobileAdapter.OnItemClickListener getAdapterListener();

        void onInit();
    }
}
