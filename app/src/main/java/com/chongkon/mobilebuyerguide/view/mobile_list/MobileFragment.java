package com.chongkon.mobilebuyerguide.view.mobile_list;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.chongkon.mobilebuyerguide.R;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;
import com.chongkon.mobilebuyerguide.tools.Enum;
import com.chongkon.mobilebuyerguide.view.BaseFragment;
import com.chongkon.mobilebuyerguide.view.mobile_detail.MobileDetailActivity;

import org.parceler.Parcels;

import java.util.ArrayList;

/*
 * Created by omkingo on 2/02/18.
 */
public class MobileFragment extends BaseFragment implements IMobile.View{

    private IMobile.Presenter mPresenter;
    private RecyclerView rv_mobile;
    private MobileAdapter mAdapter;
    private ProgressBar mLoading;

    @Override
    protected int getInflaterLayout() {
        return R.layout.fragment_mobile;
    }

    @Override
    protected void bindView() {
        rv_mobile = findViewById(R.id.rv_mobile);
        mLoading = findViewById(R.id.loading);
    }

    @Override
    protected void setupInstance() {
        mPresenter = new MobilePresenter();
        mPresenter.attachView(this);

        mAdapter = new MobileAdapter(getContext(), mPresenter.getAdapterListener());
        rv_mobile.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_mobile.setAdapter(mAdapter);
    }

    @Override
    protected void onFragmentReady() {
        mPresenter.onInit();
    }

    @Override
    public void onSetListData(ArrayList<MobileModel> list){
        mAdapter.setItem(list);
    }

    @Override
    public void onMobileDetailActivity(MobileModel model) {
        Intent intent = new Intent(getActivity(), MobileDetailActivity.class);
        intent.putExtra(Enum.Bundle.MOBILE_MODEL, Parcels.wrap(MobileModel.class, model));
        startActivity(intent);
    }

    @Override
    public void showLoading(){
        mLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading(){
        mLoading.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }
}
