package com.chongkon.mobilebuyerguide.view.mobile_list;


import com.chongkon.mobilebuyerguide.service.model.MobileModel;
import com.chongkon.mobilebuyerguide.view.MvpPresenter;
import com.chongkon.mobilebuyerguide.view.MvpView;

import java.util.ArrayList;

/*
 * Created by omkingo on 2/10/18.
 */

public interface IMobile {

    interface View extends MvpView{
        void onSetListData(ArrayList<MobileModel> list);

        void onMobileDetailActivity(MobileModel model);

        void showLoading();

        void hideLoading();
    }

    interface Presenter extends MvpPresenter<IMobile.View> {
        void onInit();

        MobileAdapter.OnItemClickListener getAdapterListener();
    }
}
