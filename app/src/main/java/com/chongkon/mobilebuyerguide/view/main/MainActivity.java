package com.chongkon.mobilebuyerguide.view.main;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.chongkon.mobilebuyerguide.R;
import com.chongkon.mobilebuyerguide.tools.DialogHelper;
import com.chongkon.mobilebuyerguide.view.BaseActivity;

import java.util.ArrayList;

/*
 * Created by omkingo on 11/27/17.
 */
public class MainActivity extends BaseActivity implements IMain.View{

    private IMain.Presenter mPresenter;
    private TabLayout tab_layout;
    private ViewPager view_pager;
    private MainPagerAdapter mAdapter;

    @Override
    protected int getLayoutView() {
        return R.layout.activity_main;
    }

    @Override
    protected void bindView() {
        tab_layout = findViewById(R.id.tab_layout);
        view_pager = findViewById(R.id.view_pager);

    }

    @Override
    protected void setupInstance() {
        mPresenter = new MainPresenter();
        mPresenter.attachView(this);

        mAdapter = new MainPagerAdapter(getSupportFragmentManager());
    }

    @Override
    protected void onActivityReady() {
        mPresenter.onInit();
    }

    @Override
    public void initialTabLayout() {
        tab_layout.setTabMode(TabLayout.MODE_FIXED);

        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                view_pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void addTab(String[] tabs){
        for (String name: tabs){
            tab_layout.addTab(tab_layout.newTab().setText(name));
        }
    }

    @Override
    public void initialViewPager(){
        view_pager.setAdapter(mAdapter);
        view_pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_layout));
    }

    @Override
    public void setFragments(ArrayList<Fragment> fragments){
        mAdapter.setFragmentInViewPager(fragments);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        // return true so that the menu pop up is opened
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort:
                mPresenter.onShowSortDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }
}
