package com.chongkon.mobilebuyerguide.view.main;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;

import com.chongkon.mobilebuyerguide.R;
import com.chongkon.mobilebuyerguide.tools.DialogHelper;
import com.chongkon.mobilebuyerguide.tools.Enum;
import com.chongkon.mobilebuyerguide.view.favorite_list.FavoriteFragment;
import com.chongkon.mobilebuyerguide.view.mobile_list.MobileFragment;

import java.util.ArrayList;

/*
 * Created by omkingo on 11/27/17.
 */
public class MainPresenter implements IMain.Presenter {

    private IMain.View mView;
    private String[] tabsName = { "MOBILE LIST", "FAVORITE LIST"};
    public static int SORT_TYPE = -1;

    @Override
    public void attachView(IMain.View view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }

    @Override
    public void onInit(){
        mView.initialTabLayout();
        mView.addTab(tabsName);
        mView.initialViewPager();
        mView.setFragments(getFragments());
    }

    private ArrayList<Fragment> getFragments(){
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new MobileFragment());
        fragments.add(new FavoriteFragment());

        return fragments;
    }

    @Override
    public void onShowSortDialog() {
        DialogHelper.showMultipleChoicesDialog(mView.getContext(), R.array.sort_options, new DialogHelper.SingleChoiceListener(){
            @Override
            public void onSelected(int position) {
                SORT_TYPE = position;
                Intent sort_intent = new Intent(Enum.REGISTER_SORT);
                sort_intent.putExtra(Enum.Bundle.SORT_SELECTED, position);
                LocalBroadcastManager.getInstance(mView.getContext()).sendBroadcast(sort_intent);
            }
        });

    }

}
