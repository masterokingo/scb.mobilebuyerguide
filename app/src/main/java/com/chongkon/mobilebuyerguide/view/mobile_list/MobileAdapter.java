package com.chongkon.mobilebuyerguide.view.mobile_list;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chongkon.mobilebuyerguide.R;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;

import java.util.ArrayList;

/*
 * Created by omkingo on 02/10/18.
 */

public class MobileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final Context mContext;
    private final OnItemClickListener listener;
    private ArrayList<MobileModel> mDataList;
    private boolean isFavoriteTab = false;

    public static class OnItemClickListener {
        public void onItemClicked(MobileModel model){}
        public void onFavoriteClicked(MobileModel model){}
        public void onRemoveFavorite(MobileModel model){}
    }

    public MobileAdapter(Context context, OnItemClickListener listener) {
        this.mContext = context;
        this.listener = listener;
    }

    public void setItem(ArrayList<MobileModel> list){
        this.mDataList = list;
        this.notifyDataSetChanged();
    }

    public void setFavoriteTab(){
        this.isFavoriteTab = true;
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mobile, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final MobileModel model = mDataList.get(position);

        Glide.with(mContext).load(model.thumbImageURL).into(((ViewHolder) holder).iv_thumbnail);
        ((ViewHolder) holder).tv_name.setText(model.name);
        ((ViewHolder) holder).tv_description.setText(model.description);
        ((ViewHolder) holder).tv_price.setText(mContext.getString(R.string.text_price, String.valueOf(model.price)));
        ((ViewHolder) holder).tv_rating.setText(mContext.getString(R.string.text_rating,  String.valueOf(model.rating)));

        ((ViewHolder) holder).setClicked(model, listener);

        if  (model.isFavorite){
            ((ViewHolder) holder).iv_favorite.setImageResource(R.drawable.ic_favorite);

        }else {
            ((ViewHolder) holder).iv_favorite.setImageResource(R.drawable.ic_favorite_border);
        }

        if (isFavoriteTab) ((ViewHolder) holder).iv_favorite.setVisibility(View.INVISIBLE);
        else ((ViewHolder) holder).iv_favorite.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        if(mDataList == null) {
            return 0;
        }
        return mDataList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private CardView card_view;
        private ImageView iv_thumbnail, iv_favorite;
        private TextView tv_name, tv_description,  tv_price, tv_rating;

        ViewHolder(View itemView) {
            super(itemView);
            card_view = itemView.findViewById(R.id.card_view);
            iv_thumbnail = itemView.findViewById(R.id.iv_thumbnail);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_description = itemView.findViewById(R.id.tv_description);
            iv_favorite = itemView.findViewById(R.id.iv_favorite);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_rating = itemView.findViewById(R.id.tv_rating);
        }

        void setClicked(final MobileModel model, final OnItemClickListener mListener) {
            card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClicked(model);
                }
            });
            iv_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if  (model.isFavorite){
                        model.isFavorite = false;
                        mListener.onRemoveFavorite(model);
                        notifyDataSetChanged();
                    }else {
                        model.isFavorite = true;
                        mListener.onFavoriteClicked(model);
                        notifyDataSetChanged();
                    }

                }
            });
        }

    }
}
