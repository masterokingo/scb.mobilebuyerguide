package com.chongkon.mobilebuyerguide.view.mobile_detail;


import com.chongkon.mobilebuyerguide.service.model.ImageModel;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;
import com.chongkon.mobilebuyerguide.view.MvpPresenter;
import com.chongkon.mobilebuyerguide.view.MvpView;

import java.util.ArrayList;

/*
 * Created by omkingo on 2/10/18.
 */

public interface IMobileDetail {

    interface View extends MvpView {
        void showLoading();

        void hideLoading();

        void setUi();

        void setImageData(ArrayList<ImageModel> modelList);
    }

    interface Presenter extends MvpPresenter<IMobileDetail.View> {
        void onInitUi(MobileModel model);
    }
}
