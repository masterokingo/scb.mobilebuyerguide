package com.chongkon.mobilebuyerguide.view.mobile_detail;

import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chongkon.mobilebuyerguide.R;
import com.chongkon.mobilebuyerguide.service.model.ImageModel;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;
import com.chongkon.mobilebuyerguide.tools.Enum;
import com.chongkon.mobilebuyerguide.view.BaseActivity;
import com.chongkon.mobilebuyerguide.view.mobile_list.MobileAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;

/*
 * Created by omkingo on 02/10/18.
 */
public class MobileDetailActivity extends BaseActivity implements IMobileDetail.View{

    private IMobileDetail.Presenter mPresenter;
    private RecyclerView rv_image;
    private TextView tv_rating, tv_price, tv_name, tv_brand, tv_description;
    private MobileModel mModel;
    private ImageAdapter mAdapter;
    private ProgressBar mLoading;

    @Override
    protected int getLayoutView() {
        return R.layout.activity_mobile_detail;
    }

    @Override
    protected void bindView() {
        rv_image = findViewById(R.id.rv_image);
        tv_rating = findViewById(R.id.tv_rating);
        tv_price = findViewById(R.id.tv_price);
        tv_name = findViewById(R.id.tv_name);
        tv_brand = findViewById(R.id.tv_brand);
        tv_description = findViewById(R.id.tv_description);
        mLoading = findViewById(R.id.loading);
    }

    @Override
    protected void setupInstance() {
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPresenter = new MobileDetailPresenter();
        mPresenter.attachView(this);

        Parcelable parcelable = getIntent().getParcelableExtra(Enum.Bundle.MOBILE_MODEL);
        if (parcelable != null)
            mModel = Parcels.unwrap(parcelable);

        mAdapter = new ImageAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_image.setLayoutManager(linearLayoutManager);
        rv_image.setAdapter(mAdapter);
    }

    @Override
    protected void onActivityReady() {
        mPresenter.onInitUi(mModel);
    }

    @Override
    public void showLoading(){
        mLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading(){
        mLoading.setVisibility(View.GONE);
    }

    @Override
    public void setUi() {
        tv_rating.setText(getString(R.string.text_rating, String.valueOf(mModel.rating)));
        tv_price.setText(getString(R.string.text_price, String.valueOf(mModel.price)));
        tv_name.setText(mModel.name);
        tv_brand.setText(mModel.brand);
        tv_description.setText(mModel.description);
    }

    @Override
    public void setImageData(ArrayList<ImageModel> modelList) {
        mAdapter.setItem(modelList);
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }
}
