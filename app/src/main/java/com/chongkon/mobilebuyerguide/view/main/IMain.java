package com.chongkon.mobilebuyerguide.view.main;


import android.support.v4.app.Fragment;

import com.chongkon.mobilebuyerguide.view.MvpPresenter;
import com.chongkon.mobilebuyerguide.view.MvpView;

import java.util.ArrayList;

/*
 * Created by omkingo on 2/10/18.
 */

public interface IMain {

    interface View extends MvpView{
        void initialTabLayout();

        void addTab(String[] tabNameList);

        void initialViewPager();

        void setFragments(ArrayList<Fragment> fragments);
    }

    interface Presenter extends MvpPresenter<IMain.View> {
        void onInit();

        void onShowSortDialog();
    }
}
