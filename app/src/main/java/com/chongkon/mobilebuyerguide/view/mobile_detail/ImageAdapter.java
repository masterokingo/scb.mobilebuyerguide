package com.chongkon.mobilebuyerguide.view.mobile_detail;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chongkon.mobilebuyerguide.R;
import com.chongkon.mobilebuyerguide.service.model.ImageModel;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;

import java.util.ArrayList;

/*
 * Created by omkingo on 02/10/18.
 */

public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final Context mContext;
    private ArrayList<ImageModel> mDataList;

    ImageAdapter(Context context) {
        this.mContext = context;
    }

    void setItem(ArrayList<ImageModel> list){
        this.mDataList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final ImageModel model = mDataList.get(position);

        Glide.with(mContext).load(model.url).into(((ViewHolder) holder).iv_mobile);
    }

    @Override
    public int getItemCount() {
        if(mDataList == null) {
            return 0;
        }
        return mDataList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_mobile;

        ViewHolder(View itemView) {
            super(itemView);
            iv_mobile = itemView.findViewById(R.id.iv_mobile);
        }

    }
}
