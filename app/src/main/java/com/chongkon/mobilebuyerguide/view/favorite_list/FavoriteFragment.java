package com.chongkon.mobilebuyerguide.view.favorite_list;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.chongkon.mobilebuyerguide.R;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;
import com.chongkon.mobilebuyerguide.tools.Enum;
import com.chongkon.mobilebuyerguide.view.BaseFragment;
import com.chongkon.mobilebuyerguide.view.mobile_detail.MobileDetailActivity;
import com.chongkon.mobilebuyerguide.view.mobile_list.MobileAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;

/*
 * Created by omkingo on 02/10/18.
 */
public class FavoriteFragment extends BaseFragment implements IFavoriteList.View{

    private IFavoriteList.Presenter mPresenter;
    private RecyclerView rv_favorite;
    private MobileAdapter mAdapter;

    @Override
    protected int getInflaterLayout() {
        return R.layout.fragment_favorite;
    }

    @Override
    protected void bindView() {
        rv_favorite = findViewById(R.id.rv_favorite);
    }

    @Override
    protected void setupInstance() {
        mPresenter = new FavoritePresenter();
        mPresenter.attachView(this);

        mAdapter = new MobileAdapter(getContext(), mPresenter.getAdapterListener());
        rv_favorite.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_favorite.setAdapter(mAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(mPresenter.getSwipeListener());
        itemTouchHelper.attachToRecyclerView(rv_favorite);
    }

    @Override
    protected void onFragmentReady() {
        mPresenter.onInit();
    }

    @Override
    public void setFavoriteTab() {
        mAdapter.setFavoriteTab();
    }

    @Override
    public void onSetListData(ArrayList<MobileModel> list){
        mAdapter.setItem(list);
    }

    @Override
    public void onMobileDetailActivity(MobileModel model) {
        Intent intent = new Intent(getActivity(), MobileDetailActivity.class);
        intent.putExtra(Enum.Bundle.MOBILE_MODEL, Parcels.wrap(MobileModel.class, model));
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }
}
