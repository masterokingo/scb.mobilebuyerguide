package com.chongkon.mobilebuyerguide.view;

import android.app.Activity;
import android.content.Context;

public interface MvpView {
    Activity getActivity();

    Context getContext();
}
