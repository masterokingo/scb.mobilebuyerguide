package com.chongkon.mobilebuyerguide.tools;

/**
 * Created by omkingo on 2/11/18.
 */

public class Enum {

    public static final String REGISTER_SORT = "SORT_BOARDCAST";
    public static final String REGISTER_FAVORITE_REMOVED = "FAVORITE_REMOVED_BOARDCAST";
    public static final String REGISTER_ADD_REMOVE_FAV = "ADD_REMOVE_FAV";

    static final int SORT_LOW_TO_HIGH = 0;
    static final int SORT_HIGH_TO_LOW = 1;
    static final int SORT_RATING = 2;

    public static class Bundle{
        public static final String SORT_SELECTED = "SORT_SELECTED";
        public static final String MOBILE_MODEL = "MOBILE_MODEL";
        public static final String IS_ADD_FAVORITE = "IS_ADD";
    }
}
