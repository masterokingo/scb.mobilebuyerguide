package com.chongkon.mobilebuyerguide.tools;

import android.content.Context;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.chongkon.mobilebuyerguide.R;

public class DialogHelper {
    public static class SingleChoiceListener{
        public void onSelected(int position){}
    }

    public static void showMultipleChoicesDialog(Context context, int itemList, final SingleChoiceListener listener){
        new MaterialDialog.Builder(context)
                .title(R.string.menu_title_sort)
                .items(itemList)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        listener.onSelected(position);
                    }
                })
                .show();
    }
}
