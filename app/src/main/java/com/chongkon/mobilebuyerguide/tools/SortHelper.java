package com.chongkon.mobilebuyerguide.tools;

import com.chongkon.mobilebuyerguide.service.model.MobileModel;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by omkingo on 2/11/18.
 */

public class SortHelper {

    public interface OnSortListener{
        void onSortCompleted(ArrayList<MobileModel> sortedList);
    }


    public static void onSortMobileList(int sortType, ArrayList<MobileModel> mList, OnSortListener listener){

        switch (sortType){
            case Enum.SORT_LOW_TO_HIGH:
                onSortLowToHigh(mList);
                break;
            case Enum.SORT_HIGH_TO_LOW:
                onSortHighToLow(mList);
                break;
            case Enum.SORT_RATING:
                onSortRating(mList);
                break;
            default:
                onSortId(mList);
                break;
        }

        listener.onSortCompleted(mList);
    }

    private static void onSortId(ArrayList<MobileModel> modelList){
        Collections.sort(modelList, MobileModel.Comparators.ID);
    }

    private static void onSortLowToHigh(ArrayList<MobileModel> modelList){
        Collections.sort(modelList, MobileModel.Comparators.PRICE_LOW_TO_HIGH);
    }

    private static void onSortHighToLow(ArrayList<MobileModel> modelList){
        Collections.sort(modelList, MobileModel.Comparators.PRICE_HIGH_TO_LOW);
    }

    private static void onSortRating(ArrayList<MobileModel> modelList){
        Collections.sort(modelList, MobileModel.Comparators.RATING);
    }
}
