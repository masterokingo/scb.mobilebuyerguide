package com.chongkon.mobilebuyerguide.service.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiBuilder {

    private static final String BASE_URL = "https://scb-test-mobile.herokuapp.com/api/";


    public ApiBuilder(){
    }

    public ApiEndpoint create() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(ApiEndpoint.class);
    }

}
