package com.chongkon.mobilebuyerguide.service.model;

/*
 * Created by omkingo on 2/10/18.
 */

import android.support.annotation.NonNull;

import com.bumptech.glide.annotation.Excludes;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Comparator;

@Parcel
public class MobileModel {

    /**
     * description : First place in our list goes to the excellent Moto E4 Plus. It's a cheap phone that features phenomenal battery life, a fingerprint scanner and a premium feel design, plus it's a lot cheaper than the Moto G5 below. It is a little limited with its power, but it makes up for it by being able to last for a whole two days from a single charge. If price and battery are the most important features for you, the Moto E4 Plus will suit you perfectly.
     * price : 179.99
     * brand : Samsung
     * thumbImageURL : https://cdn.mos.cms.futurecdn.net/grwJkAGWQp4EPpWA3ys3YC-650-80.jpg
     * id : 1
     * rating : 4.9
     * name : Moto E4 Plus
     */

    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("price")
    @Expose
    public double price;
    @SerializedName("brand")
    @Expose
    public String brand;
    @SerializedName("thumbImageURL")
    @Expose
    public String thumbImageURL;
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("rating")
    @Expose
    public double rating;
    @SerializedName("name")
    @Expose
    public String name;

    public boolean isFavorite;

    @Override
    public boolean equals(Object object)
    {
        boolean sameSame = false;

        if (object != null && object instanceof MobileModel)
        {
            sameSame = this.id == ((MobileModel) object).id;
        }

        return sameSame;
    }

    public static class Comparators {

        public static Comparator<MobileModel> ID = new Comparator<MobileModel>() {
            @Override
            public int compare(MobileModel obj1, MobileModel obj2) {
                return obj1.id - obj2.id;
            }
        };

        public static Comparator<MobileModel> PRICE_HIGH_TO_LOW = new Comparator<MobileModel>() {
            @Override
            public int compare(MobileModel obj1, MobileModel obj2) {
                return Double.compare(obj2.price, obj1.price);
            }
        };

        public static Comparator<MobileModel> PRICE_LOW_TO_HIGH = new Comparator<MobileModel>() {
            @Override
            public int compare(MobileModel obj1, MobileModel obj2) {
                return Double.compare(obj1.price, obj2.price);
            }
        };

        public static Comparator<MobileModel> RATING = new Comparator<MobileModel>() {
            @Override
            public int compare(MobileModel obj1, MobileModel obj2) {
                return Double.compare(obj2.rating, obj1.rating);
            }
        };
    }
}
