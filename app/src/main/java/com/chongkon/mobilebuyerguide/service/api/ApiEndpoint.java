package com.chongkon.mobilebuyerguide.service.api;

import com.chongkon.mobilebuyerguide.service.model.ImageModel;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiEndpoint {

    @GET("mobiles")
    Observable<ArrayList<MobileModel>> getList();

    @GET("mobiles/{mobile_id}/images")
    Observable<ArrayList<ImageModel>> getMobileDetailImage(@Path("mobile_id") int mobile_id);

}