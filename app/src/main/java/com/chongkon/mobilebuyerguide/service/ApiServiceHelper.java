package com.chongkon.mobilebuyerguide.service;

import android.content.Context;

import com.chongkon.mobilebuyerguide.service.api.ApiBuilder;
import com.chongkon.mobilebuyerguide.service.model.ImageModel;
import com.chongkon.mobilebuyerguide.service.model.MobileModel;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ApiServiceHelper {

    private Context mContext;
    private CompositeDisposable mComDisposable;
    private ApiBuilder mApiBuilder;

    public ApiServiceHelper(Context context) {
        mContext = context;
        mComDisposable = new CompositeDisposable();
        mApiBuilder = new ApiBuilder();
    }

    public static class ApiCallback{
        public void onResponse(ArrayList<?> model){}
        public void onFailure(String msg){}
    }

    public void callGetMobileList(final ApiCallback mCallback){
        Disposable disposable = mApiBuilder.create().getList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ArrayList<MobileModel>>() {
                    @Override
                    public void accept(ArrayList<MobileModel> list) throws Exception {
                        mCallback.onResponse(list);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mCallback.onFailure(throwable.getMessage());
                    }
                });

        mComDisposable.add(disposable);
    }

    public void callGetImagePath(int mobile_id, final ApiCallback mCallback){
        Disposable disposable = mApiBuilder.create().getMobileDetailImage(mobile_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ArrayList<ImageModel>>() {
                    @Override
                    public void accept(ArrayList<ImageModel> list) throws Exception {
                        mCallback.onResponse(list);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mCallback.onFailure(throwable.getMessage());
                    }
                });

        mComDisposable.add(disposable);
    }

    public void onDispose(){
        if (mComDisposable != null)
            mComDisposable.dispose();

        mComDisposable = null;
    }
}
