package com.chongkon.mobilebuyerguide.service.model;

/**
 * Created by omkingo on 2/11/18.
 */

public class ImageModel {

    /**
     * mobile_id : 1
     * id : 1
     * url : https://www.91-img.com/gallery_images_uploads/f/c/fc3fba717874d64cf15d30e77a16617a1e63cc0b.jpg
     */

    public int mobile_id;
    public int id;
    public String url;
}
